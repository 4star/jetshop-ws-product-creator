### What is this repository for? ###

This application uses Jetshops Webservice to create products with some random data in a shop. 
It's a quick demo on how to use Product_Save and how to upload images and attach them to products.

No, the code is not nice but the purpose is to show how Product_Save works.

### How do I use it? ###

* Load up the solution
* Run project
* Choose server environment (most are on ITG)
* Enter shop webservice credentials
* Choose amount of products and if they should have attributes
* Watch the magic
