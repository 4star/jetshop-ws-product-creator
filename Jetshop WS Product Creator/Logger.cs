﻿namespace Jetshop_WS_Product_Creator
{
    using System;
    using System.IO;
    using System.Reflection;

    public class Logger
    { 
    private const string Path = @"C:\temp\";
    private const string Logfile = "productCreatorLog.txt";

        public Logger()
        {
            CreateLogFile();
        }

        public void DisplayVersionInfo()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"Jetshop Webservice Product Creator - Version: {Assembly.GetExecutingAssembly().GetName().Version}");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("\n-----------------------------------\n");
        }

        private void CreateLogFile()
        {
            if (!Directory.Exists(Path))
            {
                Directory.CreateDirectory(Path);
            }

            if (!File.Exists(Path + Logfile))
            {
                File.Create(Path + Logfile).Close();
            }
            
            LogMessage("Program started");
        }

        public void LogMessage(string msg)
        {
            var date = DateTime.Today.Date.ToString("d");
            var time = DateTime.Now.ToString("HH:mm:ss tt");
            var writer = new StreamWriter(@"C:\temp\productCreatorLog.txt", true);
            writer.WriteLine(date + " " + time + " : " + msg);
            writer.Close();
        }

        public void LogError(string error)
        {
            var date = DateTime.Today.Date.ToString("d");
            var time = DateTime.Now.ToString("HH:mm:ss tt");
            var writer = new StreamWriter(@"C:\temp\productCreatorLog.txt", true);
            writer.WriteLine(date + " " + time + " : " + " --ERROR-- " + error);
            writer.Close();
        }

    }
}
