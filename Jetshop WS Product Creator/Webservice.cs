﻿namespace Jetshop_WS_Product_Creator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading;
    using se.jetshop.integration;

    public class Webservice
    {
        private Logger Logger = new Logger();
        public string Url { get; set; }
        public JetshopWS WebService { get; set; }
        private string Username { get; set; }
        private string Password { get; set; }

        public string TestAndReturnConnection()
        {
            var ws = new JetshopWS();
            WebService = ws;

            Console.CursorVisible = false;
            Console.WriteLine("Choose serverenvironment:\n(1 = ITG | 2 = PIN | * = TEST)");

            var key = Console.ReadKey();
            var serverenvironment = char.IsDigit(key.KeyChar) ? int.Parse(key.KeyChar.ToString()) : 0;

            try
            {
                switch (serverenvironment)
                {
                    case 1:
                        ws.Url = "https://integration.jetshop.se/Webservice20/v3.0/webservice.asmx";
                        Url = ws.Url;
                        break;
                    case 2:
                        ws.Url = "http://webservices20.jetshop.se/api3.0/WebService.asmx";
                        Url = ws.Url;
                        break;

                    default:
                        ws.Url = "http://integrationtest.jetshop.se/webservice20/v3.0-test/webservice.asmx";
                        Url = ws.Url;
                        break;
                }
            }

            catch (Exception e)
            {
                Logger.LogError(e.Message + $"\nStacktrace: {e.StackTrace}");
                Console.WriteLine(e);
                throw;
            }
            
            Console.WriteLine($"\bSelected server: {Url}\n");
            Console.CursorVisible = true;
            Console.WriteLine("Enter username:");
            var userName = Console.ReadLine();
            Console.WriteLine("Enter password:");
            var password = GetConsolePassword();

            if (!TestCredentials(userName, password))
            {
                Url = string.Empty;
                return Url;
            }

            Logger.LogMessage($"Webservice user {userName} connected at: {Url}");
            return Url;
        }

        private bool TestCredentials(string userName, string password)
        {
            var isConnected = false;

            var ws = new JetshopWS
            {
                Credentials = new NetworkCredential(userName, password),
                Url = Url,
                Timeout = 6000000,
                CookieContainer = new CookieContainer()
            };

            try
            {
                var request = WebRequest.Create(Url);
                request.Credentials = ws.Credentials;

                using (request.GetResponse())
                {
                    ws.GetChannels();
                    Console.WriteLine($"\n{userName} succesfully connected to the webservice at {Url}.");
                    Username = userName;
                    Password = password;
                    isConnected = true;
                }
            }

            catch (WebException e)
            {
                using (var response = e.Response)
                {
                    Console.WriteLine(
                        "\nCheck that the username and password is correct!\nIf running local make sure WebserviceProvider and CBA is running.");
                    var httpResponse = (HttpWebResponse) response;
                    if (response != null)
                    {
                        Console.WriteLine($"Error code: {httpResponse.StatusCode} - {httpResponse.StatusDescription}");
                        Logger.LogError(
                            e.Message +
                            $"\nHttpResponse: {httpResponse.StatusCode} - {httpResponse.StatusDescription}");
                    }

                    Logger.LogError(e.Message + $"\nStacktrace: {e.StackTrace}");
                    Console.WriteLine(e.Message);
                }
            }

            catch (Exception e)
            {
                Logger.LogError(e.Message + $"\nStacktrace: {e.StackTrace}");
                Console.WriteLine(e.Message);
                Console.ReadKey();
                Console.Clear();
                Logger.DisplayVersionInfo();
                TestAndReturnConnection();
            }
            
            WebService = ws;
            return isConnected;
        }

        private static string GetConsolePassword()
        {
            var sb = new StringBuilder();
            while (true)
            {
                var cki = Console.ReadKey(true);
                if (cki.Key == ConsoleKey.Enter)
                {
                    Console.WriteLine();
                    break;
                }

                if (cki.Key == ConsoleKey.Backspace)
                {
                    if (sb.Length > 0)
                    {
                        Console.Write("\b\0\b");
                        sb.Length--;
                    }

                    continue;
                }

                Console.Write('*');
                sb.Append(cki.KeyChar);
            }

            return sb.ToString();
        }

        public void InsertProducts()
        {
            try
            {
                using (WebService)
                {
                    WebService.Credentials = new NetworkCredential(Username, Password);
                    Console.WriteLine("\nNumber of products?");
                    var productAmount = Console.ReadLine();
                    var productsToInsert = int.Parse(productAmount ?? throw new InvalidOperationException());


                    Console.WriteLine("\nFetching categories...\n");
                    Console.CursorVisible = false;
                    var categories = GetAllCategories();

                    foreach (var cat in categories)
                    {
                        Console.WriteLine($"Name: {cat.Name} -- ID: {cat.CategoryId}");
                    }

                    Console.WriteLine("\nCategoryID?");
                    Console.CursorVisible = true;
                    var catId = 0;
                    var catChoice = Console.ReadLine();
                    
                    if (int.TryParse(catChoice, out var n))
                    {
                        var activeCategory = categories.Where(a => a.CategoryId > 1).Any(a => a.CategoryId == n);
                        catId = activeCategory ? n : GetFirstCategory();
                    }
                    else
                    {
                        throw new InvalidOperationException();
                    }

                    Console.WriteLine("\nShould there be attributes? 1 = YES || 2 = NO");
                    var attributes = false;
                    Console.CursorVisible = false;
                    var attributechoice = Console.ReadKey();
                    if (attributechoice.Key == ConsoleKey.NumPad1 || attributechoice.Key == ConsoleKey.D1)
                    {
                        attributes = true;
                    }

                    var randomizer = new Randomizer();
                    var productsToCreate = new List<ProductSaveRequest>();
                    var articleNumbers = new List<string>();
                    var articleNumberImageList = new List<ArticleNumberImages>();

                    var defculture = GetDefaultCulture();
                    var channel = GetDefaultChannel();
                    var pricelist = channel.PriceLists.Single(a => a.IsDefaultInChannel);

                    for (var i = 0; i < productsToInsert; i++)
                    {
                        var p = new ProductSaveRequest
                        {
                            ArticleNumber = randomizer.RandomString(10),
                            GtinEan = randomizer.RandomNumericString(12)
                        };
                        
                        var priceListData = new ProductPriceList
                        {
                            Price = randomizer.RandomDecimal(),
                            VatRate = (decimal?)0.25,
                            Id = pricelist.PriceListId
                        };

                        var loc = new List<Localization>();
                        var locName = new List<Localization>();
                        var locShortDesc = new List<Localization>();
                        var desc = new Localization { Culture = defculture, Value = randomizer.RandomSentence() };
                        var name = new Localization { Culture = defculture, Value = randomizer.RandomProductName() };
                        var shortDesc = new Localization {Culture = defculture, Value = "Images by pexels.com" };

                        locShortDesc.Add(shortDesc);
                        loc.Add(desc);
                        locName.Add(name);

                        var stock = new ProductStockStatus
                            { Id = 2 };

                        p.StockStatus = stock;

                        var attrData = new List<ProductAttributeLevel1>();
                        var attrLoc = new List<Localization>();
                        var attrPrice = new List<ProductAttributePriceList>();
                        var values = new List<Localization>();

                        var prloc = new ProductLocalizations
                        {
                            Description = loc.ToArray(),                            
                            Name = locName.ToArray(),
                            ShortDescription = locShortDesc.ToArray()
                        };

                        p.Localizations = prloc;

                        ProductPriceList[] pr = { priceListData };
                        p.PriceLists = pr;

                        var artImage = new ArticleNumberImages
                        {
                            ArticleNumber = p.ArticleNumber,
                            Reload = true
                        };

                        articleNumberImageList.Add(artImage);
                        articleNumbers.Add(p.ArticleNumber);

                        var prodCat = new List<ProductCategory>();
                        var cat = new ProductCategory { Id = catId };                        
                        prodCat.Add(cat);
                        var cat2 = new ProductCategory { Id = 190 };
                        prodCat.Add(cat2);
                        var catData = new ProductCategoryData { Categories = prodCat.ToArray() };

                        p.CategoryData = catData;
                        p.CategoryData.CanonicalCategoryId = catId;

                        if (attributes)
                        {
                            var attrHead = new Localization
                            {
                                Culture = defculture,
                                Value = randomizer.RandomAttributeName()
                            };

                            for (var h = 0; h < randomizer.RandomInteger(); h++)
                            {
                                var attrPriceData = new ProductAttributePriceList
                                {
                                    Price = randomizer.RandomDecimal(),
                                    Id = pricelist.PriceListId
                                };

                                attrPrice.Add(attrPriceData);

                                var valloc = new Localization
                                {
                                    Culture = defculture,
                                    Value = randomizer.RandomNumericString(2)
                                };

                                var stockstatus = new ProductAttributeStockStatus
                                {
                                    Id = 1,
                                    StockCount = randomizer.RandomInteger()
                                };

                                values.Add(valloc);

                                var attr1 = new ProductAttributeLevel1
                                {
                                    ArticleNumber = p.ArticleNumber + "-" + randomizer.RandomNumericString(4),
                                    StockStatus = stockstatus,
                                    PriceLists = attrPrice.ToArray(),
                                    Values = values.ToArray()
                                };

                                attrData.Add(attr1);
                                values.Clear();
                                attrPrice.Clear();

                            }

                            attrLoc.Add(attrHead);

                            var attrLoca = new ProductAttributesData
                            {
                                AttributeHeaderLevel1 = attrLoc.ToArray(),  
                                Attributes = attrData.ToArray(),
                            };

                            p.AttributeData = attrLoca;

                        }
                        
                        productsToCreate.Add(p);

                        Thread.Sleep(200);
                    }

                    var imagelist = new List<ImageFileOptions>();

                    var keyword = "";

                    Console.CursorVisible = true;

                    Console.WriteLine("\n\nKeyword for images? (Leave empty for random images)");
                    keyword = Console.ReadLine();

                    if (string.IsNullOrWhiteSpace(keyword))
                    {
                        keyword = "None";
                    }

                    Console.CursorVisible = false;

                    foreach (var artNumber in articleNumbers)
                    {
                        var image = new ImageFileOptions
                        {
                            FileName = $"{artNumber}.jpg",
                            ImageByte = Convert.FromBase64String(randomizer.RandomImage(keyword)),
                            ImageName = $"{artNumber}",
                            AltText = $"{artNumber}",
                            ImageCategory = "1"
                        };
                        imagelist.Add(image);
                    }

                    var products = productsToCreate.ToArray();
                    var images = imagelist.ToArray();

                    var articleNumberImages = articleNumberImageList.ToArray();

                    Console.WriteLine("\n-----------------------------------\n");

                    foreach (var image in images)
                    {
                        Logger.LogMessage($"{image.FileName} uploaded.");
                        WebService.UploadImage(images);
                        Console.WriteLine($"Image {image.FileName} uploaded!");

                    }

                    Console.WriteLine("\n-----------------------------------\n");

                    foreach (var product in products)
                    {
                        Logger.LogMessage($"{product.Localizations.Name[0].Value} - (Art: {product.ArticleNumber}) created.");
                        WebService.Product_Save(products);
                        Console.WriteLine($"Product {product.Localizations.Name[0].Value} created!");
                    }

                    WebService.Product_AddUpdateImages(articleNumberImages, "");

                    Console.WriteLine("\n-----------------------------------\n");
                    Console.WriteLine("Great success.");
                    Console.ReadLine();
                }
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message + $"\nStacktrace: {e.StackTrace}");
                Console.WriteLine($"\n{e.Message}");
            }
        }        

        private int GetFirstCategory()
        {
            var categories = WebService.Category_Get(new CategoryOptions());
            var firstCategory = categories.Where(a => a.Culture == GetDefaultCulture().ToString())
                .First(b => b.CategoryId > 1);
            
            var categoryId = firstCategory.CategoryId;
            
            return categoryId;
        }

        private List<CategoryData> GetAllCategories()
        {
            var categories = WebService.Category_Get(new CategoryOptions());
            var listOfCategories = categories.Where(a => a.Culture == GetDefaultCulture());

            return listOfCategories.ToList();
        }

        private string GetDefaultCulture()
        {
            var cultures = WebService.GetCultures();
            var defculture = cultures.Single(a => a.IsDefault).Culture;
            return defculture;
        }

        private Channel GetDefaultChannel()
        {
            var channel = WebService.GetChannels().Single(a => a.IsDefault);
            return channel;
        }
    }
}
