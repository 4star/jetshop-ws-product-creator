﻿namespace Jetshop_WS_Product_Creator
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Runtime.InteropServices;
    using System.Text;
    using Newtonsoft.Json;

    public class Randomizer
    {
        private static readonly Random Random = new Random();
        private readonly List<string> _attributeNames = new List<string> { "Color", "Size", "Value", "Option"} ;

        public string RandomImage(string keyword)
        {
;           var urls = PexelImageUrls(keyword);
            var rnd = new Random();

            var randomUrl = rnd.Next(0, urls.Count - 1);

            var luckyUrl = urls[randomUrl];
            var image = GetImage(luckyUrl);
            using (var ms = new MemoryStream(image))
            {
                var base64 = Convert.ToBase64String(ms.ToArray());
                return base64;
            }
        }
        
        public List<string> PexelImageUrls(string keyword)
        {
            var urls = new List<string>();

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization =
                    new AuthenticationHeaderValue("563492ad6f917000010000015181c1934d9a40ac8445cad4b3e00bbc");

                if (keyword == "None")
                {
                    client.BaseAddress = new Uri("https://api.pexels.com/v1/popular?per_page=30&page=1");
                }

                else
                {
                    var uri = "https://api.pexels.com/v1/search?query=" + keyword + "&per_page=30&page=1";
                    client.BaseAddress = new Uri(uri);
                }

                var response = client.GetAsync("").Result;
                if (response.IsSuccessStatusCode)
                {
                    var contents = response.Content.ReadAsStringAsync();
                    var json = JsonConvert.DeserializeObject<RootObject>(contents.Result);
                    foreach (var photo in json.photos)
                    {
                        urls.Add(photo.src.large);
                    }
                }

                else
                {
                    Console.WriteLine("Pexels error.");
                    Console.ReadKey();
                    Console.Clear();
                    var webservice = new Webservice();
                    var logger = new Logger();
                    logger.DisplayVersionInfo();
                    webservice.TestAndReturnConnection();
                }

                return urls;
            }
        }
        
        private byte[] GetImage(string url)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            var response = (HttpWebResponse)request.GetResponse();

            using (var dataStream = response.GetResponseStream())
            {
                if (dataStream == null)
                    return null;

                var bytes = ReadAllBytes(dataStream);
                return bytes;
            }
        }

        public byte[] ReadAllBytes(Stream stream)
        {
            using (var ms = new MemoryStream())
            {
                stream.CopyTo(ms);
                return ms.ToArray();
            }
        }

        public string RandomAttributeName()
        {
            var attributeName = _attributeNames.OrderBy(a => Guid.NewGuid()).First();
            return attributeName;
        }

        public string RandomProductName()
        {
            string[] adjectives = { "Awesome", "Genious", "Impeccable", "Glorious", "Broken", "Fully equipped",
                "Somewhat used", "Slightly evil", "Kinky", "Wet", "Funny", "Original", "Out of date", "Old",
                "Brand new", "Cheeky", "Fluffy", "Over-sized", "Extra crispy", "Very shiny", "World's smallest", "Young",
                "Fat", "Skinny", "Moist", "Legacy", "Smelly", "Short", "Flirty", "Pro"
            };

            string[] nouns = { "ball", "meme", "frisebee", "application specialist", "developer", "saas platform",
                "designer", "devops person", "chicken", "female dog", "male kitty cat", "gangster", "yesman", "knug",
                "support technician", "gamer", "champion", "swag", "violin", "digital currency", "customer success manager",
                "undefined being", "stick", "product", "ninja", "project manager", "web designer"
            };

            var rnd = new Random();

            var adjective = adjectives[rnd.Next(0, adjectives.Length)];
            var noun = nouns[rnd.Next(0, nouns.Length)];

            var productName = $"{adjective} {noun}";

            return productName;
        }

        public string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[Random.Next(s.Length)]).ToArray());
        }

        public string RandomNumericString(int length)
        {
            const string chars = "0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[Random.Next(s.Length)]).ToArray());
        }

        public decimal RandomDecimal()
        {
            var number = Math.Round((Random.NextDouble() * 10001), 2);
            return (decimal) number;
        }

        public int RandomInteger()
        {
            var rnd = new Random();
            var number = rnd.Next(1, 10);
            return number;
        }

        public string RandomSentence()
        {
            string[] words = { "thrilling", "dog", "man", "the", "for",
                "and", "a", "with", "bird", "fox", "Jetshop", "saas",
                "what", "ecommerce", "sense", "illogical", "random",
                "summer", "wiretap", "google", "whatever", "something",
                "daring", "passionate", "commited", "Ambitious", "buzz",
                "creates", "awesome", "product", "flight", "tech", "savvy"
            };

            var text = new RandomText(words);

            text.AddContentParagraphs(2, 2, 4, 15, 50);
            var content = text.Content;
            return content;
        }

        public string GenerateBase64ImageString()
        {
            using (var bitmap = new Bitmap(400, 400, PixelFormat.Format24bppRgb))
            {
                var data = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.WriteOnly, bitmap.PixelFormat);

                var noise = new byte[data.Width * data.Height * 3];
                new Random().NextBytes(noise);
                Marshal.Copy(noise, 0, data.Scan0, noise.Length);

                bitmap.UnlockBits(data);

                using (var jpegStream = new MemoryStream())
                {
                    bitmap.Save(jpegStream, ImageFormat.Jpeg);
                    var base64 = Convert.ToBase64String(jpegStream.ToArray());
                    return base64;
                }
            }
        }

        public class RandomText
        {
            public static readonly Random Random = new Random();
            public readonly StringBuilder Builder;
            public readonly string[] Words;

            public RandomText(string[] words)
            {
                Builder = new StringBuilder();
                Words = words;
            }

            public void AddContentParagraphs(int numberParagraphs, int minSentences,
                int maxSentences, int minWords, int maxWords)
            {
                for (var i = 0; i < numberParagraphs; i++)
                {
                    AddParagraph(Random.Next(minSentences, maxSentences + 1),
                        minWords, maxWords);
                    Builder.Append("\n\n");
                }
            }

            void AddParagraph(int numberSentences, int minWords, int maxWords)
            {
                for (var i = 0; i < numberSentences; i++)
                {
                    var count = Random.Next(minWords, maxWords + 1);
                    AddSentence(count);
                }
            }
            
            void AddSentence(int numberWords)
            {
                var b = new StringBuilder();
                for (var i = 0; i < numberWords; i++) 
                {
                    b.Append(Words[Random.Next(Words.Length)]).Append(" ");
                }
                var sentence = b.ToString().Trim() + ". ";
                sentence = char.ToUpper(sentence[0]) + sentence.Substring(1);
                Builder.Append(sentence);
            }

            public string Content => Builder.ToString();
            
        }

        public class Src
        {
            public string original { get; set; }
            public string large2x { get; set; }
            public string large { get; set; }
            public string medium { get; set; }
            public string small { get; set; }
            public string portrait { get; set; }
            public string square { get; set; }
            public string landscape { get; set; }
            public string tiny { get; set; }
        }

        public class Photo
        {
            public int id { get; set; }
            public int width { get; set; }
            public int height { get; set; }
            public string url { get; set; }
            public string photographer { get; set; }
            public Src src { get; set; }
        }

        public class RootObject
        {
            public int page { get; set; }
            public int per_page { get; set; }
            public List<Photo> photos { get; set; }
            public string next_page { get; set; }
        }
    }
}
