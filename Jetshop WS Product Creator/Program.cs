﻿namespace Jetshop_WS_Product_Creator
{
    using System;

    public class Program
    {
        static void Main()
        {
            var webservice = new Webservice();
            var logger = new Logger();
            
            while (true)
            {
                Console.Clear();
                logger.DisplayVersionInfo();
                webservice.TestAndReturnConnection();
                webservice.InsertProducts();
                Console.ReadKey();
            }
        }
    }
}
